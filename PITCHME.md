<!--

[ ] Quickstart: indent properties correctly
[ ] add drawing that presents global view (incl. external components)
[ ] fix CI chain in training-session-01
[ ] add generator part
[ ] add deployment part

-->

## Training: Application Onboarding Process
### Session 1

#HSLIDE

## Agenda for today

<ol>
<li><a href="#/1">Requirements</a></li>
<!-- .element: class="fragment" -->

<li><a href="#/2">Definitions</a></li><!-- .element: class="fragment" -->

<li><a href="#/3">Learning objectives</a></li><!-- .element: class="fragment" -->

<li><a href="#/4">Lab walkthrough</a></li><!-- .element: class="fragment" -->

<li><a href="#/5">Why TOSCA?</a></li><!-- .element: class="fragment" -->

<li><a href="#/6">Create a sample VNF step by step</a></li><!-- .element: class="fragment" -->

<li><a href="#/7">`generator` - From TOSCA to HOT</a></li><!-- .element: class="fragment" -->

<li><a href="#/8">Deploy a VNF</a></li><!-- .element: class="fragment" -->

<li><a href="#/9">It's your turn: collecting requirements</a></li><!-- .element: class="fragment" -->
</ol>

#HSLIDE

## 1. Requirements

To get the most out of the training, you

- <!-- .element: class="fragment" --> have a linux environment and are not afraid of `bash`
- <!-- .element: class="fragment" --> have used `ssh` before
- <!-- .element: class="fragment" --> have already sent us the data about your VNF we requested earlier
- <!-- .element: class="fragment" --> know how to `git` `clone`, `commit`, `push`, `pull`
- <!-- .element: class="fragment" --> read the [Openstack](https://docs.openstack.org/security-guide/introduction/introduction-to-openstack.html) introduction
- <!-- .element: class="fragment" --> actually WANT to get your app on-board


<!-- .element: class="fragment" style="font-size:50%" --> Now you know your homework...

#HSLIDE

## 2. Definitions

<!-- .element: class="fragment" --> YAML: Yet another markup language; see <a href="#/yaml">below</a>

<!-- .element: class="fragment" --> VNFD: VNF descriptor; abstract information about the application and its requirements

<!-- .element: class="fragment" --> Repository (repo): Git database stored on our Gitlab servers that enables version control
and automated deployment on push

 <!-- .element: class="fragment" --> HOT: Heat Orchestration Template; Openstack-specific YAML files that describe a Stack;
can be deployed with [`openstack stack-create`](https://docs.openstack.org/user-guide/cli-create-and-manage-stacks.html)

<!-- .element: class="fragment" --> Toolchain: our set of tools that enable you to efficiently validate, commit and deploy VNFDs;
available in your VNF repository (to be created)

<!-- .element: class="fragment" --> `Generator`: a Python script that converts a TOSCA VNFD into Openstack HOT files

#HSLIDE

## 3. Learning objectives

At the end of the training, you

- <!-- .element: class="fragment" --> know why we use [TOSCA](http://docs.oasis-open.org/tosca/TOSCA-Simple-Profile-YAML/v1.0/csprd01/TOSCA-Simple-Profile-YAML-v1.0-csprd01.html) to describe applications
- <!-- .element: class="fragment" --> understand the lab architecture
- <!-- .element: class="fragment" --> can create a simple VNF descriptor in TOSCA
- <!-- .element: class="fragment" --> are able to deploy the VNFD to our lab
- <!-- .element: class="fragment" --> can use the `generator` to generate Openstack HOT files from the VNF

#HSLIDE

## 4. Lab walkthrough

#VSLIDE

### 10,000 feet

![lab](assets/lab-sol-b.png)

#VSLIDE

### Down on earth

<!--  TODO: provide HOWTO document -->
To access the lab, please follow the directions in the document `HOWTO` (will send)

#HSLIDE

## 5. TOSCA & YAML

### What is it used for in our context?

#VSLIDE

<a name="yaml"></a>
### YAML intro

- human-readable data serialization
- tree of elements based on whitespaces
- implicit data types
- every space counts (we use two-spaces indentation)

#VSLIDE

#### YAML example

```yml
key: value
boolean: true
string: "hello world"
dictionary:
  key: value
array:
  - val1
  - val2
```


#VSLIDE

### TOSCA

> TOSCA specification provides a **language to describe service components and their relationships** using a service topology.

<!-- .element style="font-size:50%" --> Further reading:
- <!-- .element style="font-size:50%" --> [TOSCA specs](http://docs.oasis-open.org/tosca/TOSCA/v1.0/os/TOSCA-v1.0-os.html)
- <!-- .element style="font-size:50%" --> [TOSCA Simple Profile YAML](http://docs.oasis-open.org/tosca/TOSCA-Simple-Profile-YAML/v1.0/csprd01/TOSCA-Simple-Profile-YAML-v1.0-csprd01.html)

#VSLIDE

Topology<br/>
and<br/>
Orchestration<br/>
Specification<br/>
for<br/>
Cloud<br/>
Applications<br/>

#VSLIDE

#### TOSCA is great because

we abstract description and implementation

<span style="font-size:50%">(side note: Openstack HOT is also YAML, but tied to Openstack)</span>

#VSLIDE

![generator](assets/generator.png)

#VSLIDE

#### `dtag` extension - basics

Required elements for every node type:

```yml
properties:
  name: [name]
  version: [version]
  description: [description]
```

#VSLIDE

#### `dtag` extension - Node types

We use slightly different `node_types` to reflect our needs, like

- `tosca.dtag.nodes.VNF`
- `tosca.dtag.nodes.Tenant`
- `tosca.dtag.nodes.Component`
- `tosca.dtag.nodes.ExternalComponent`
- `tosca.dtag.nodes.InternalComponent`
- `tosca.dtag.nodes.Network`


#VSLIDE

#### `dtag` extension - linking components

Components expose either requirements, capabilities or none.

![TOSCA Components](/assets/Tosca-Component.svg) <!-- .element height="60%" width="60%" -->


#HSLIDE

## 6. VNFD quick start

#VSLIDE

Let's build something cool... like a **webserver**!

So we need:
- <!-- .element: class="fragment" --> access to DNS (`UDP:53`)
- <!-- .element: class="fragment" --> access to proxy (`TCP:3128`)
- <!-- .element: class="fragment" --> expose web server (`TCP:80`)
- <!-- .element: class="fragment" --> expose SSH (`TCP:22`)

#VSLIDE

### Network design

<!-- TODO: align names -->

![network_diagram](assets/network.png)

#VSLIDE

### 1. Fire up your favorite editor!

![editor](/assets/editor.jpg) <!-- .element height="50%" width="50%" -->

#VSLIDE

### 2. Add the skeleton

Every TOSCA file needs those generic lines.

```yml
tosca_definitions_version: TOSCA_dtag_profile_for_nfv_0_0_4

description: VNF Template for the reference tenant.

# Topology Template

topology_template:
  node_templates:
```

#VSLIDE

### 3. Add a VNF

First, we are adding a VNF called `VNF_training-session-01`

```yml
...

topology_template:
  node_templates:
  VNF_training-session-01:
    type: tosca.dtag.nodes.VNF
    properties:
      name:        VNF_training-session-01
      version:     '0.9'
      description: training-session-01 tenant
      vendor:      AT2
```

#VSLIDE

### 4. Add a datacenter

A VNF is contained in a datacenter which is created here. Refer to our [dtag tosca specs](http://172.16.2.210/application_orchestration/vnfd_engine/blob/development/docs/tosca.md) for a description of the properties

```yml
...

topology_template:
  node_templates:
    ...
    training-session-01:
      type: tosca.dtag.nodes.Tenant
      properties:
        name:        training-session-01
        version:     '1.0'
        description: training-session-01 tenant in SOL-B data center
        vnf:         VNF_training-session-01
        datacenter:  SOL-B
        ipv6_prefix: 'fdfd:1:1:b208::' # todo
        keys:  |
          ssh-rsa ...
```


#VSLIDE

### 5. Add networks (1)

Public service network

```yml
topology_template:
  node_templates:
    ...
    svc1:
      type: tosca.dtag.nodes.Network
      properties:
        name:         svc1
        version:      '1.0'
        description:  Public ipv4
        tenant:       training-session-01
        target:       '64512:42'
        ipv4:
          cidr:       198.18.215.0/28
          gateway:    198.18.215.1
          start:      198.18.215.5
          end:        198.18.215.14
```

#VSLIDE

### 5. Add networks (2)

Package repository network

```yml
topology_template:
  node_templates:
    ...
    svc2:
      type: tosca.dtag.nodes.Network
      properties:
        name:         svc2
        version:      '1.0'
        description:  Expose repository services
        tenant:       training-session-01
        target:       '64512:42'
        ipv4:
          cidr:       198.18.215.16/28
          gateway:    198.18.215.17
          start:      198.18.215.20
          end:        198.18.215.30
```

#VSLIDE

### 5. Add networks (3)

OAM network

```yml
topology_template:
  node_templates:
    ...
    oam:
      type: tosca.dtag.nodes.Network
      properties:
        name:         oam
        version:      '1.0'
        description:  OAM network
        tenant:       training-session-01
        ipv4:
          cidr:       192.168.0.0/24
          gateway:    192.168.0.1
          start:      192.168.0.10
          end:        192.168.0.250
```


#VSLIDE

### 6. Add external components (1)

We want an administrator that has SSH access to the webserver.

```yml
topology_template:
  node_templates:
    ...
    oandm:
      type: tosca.dtag.nodes.ExternalComponent
      properties:
        name:        oandm
        version:     '1.0'
        description: administrator of training-session-01
        network:     oam
        ipv4:
          - 0.0.0.0/0
      requirements:
        - server3_ssh:
            node_filter:
              properties:
              -  component:  server3
                 capability: ssh
```

#VSLIDE

### 6. Add external components (2)

Clients (users or servers) need to access our webserver.

```yml
topology_template:
  node_templates:
    ...
    tenants:
      type: tosca.dtag.nodes.ExternalComponent
      properties:
        name:        tenants
        version:     '1.0'
        description: Expose services
        network:     svc2
        ipv4:
          - 0.0.0.0/16
      requirements:
        - proxy_http:
            node_filter:
              properties:
              -  component:  server1
                 capability: web
```

#VSLIDE

### 7. Add virtual machines - `server1` (1)

Now, let's add the server that provides common functionality.

```yml
topology_templates:
  node_templates:
   ...
   server1:
      type: tosca.dtag.nodes.InternalComponent
      properties:
        name:         server1
        version:      '1.0'
        description:  server1 server
        tenant:       training-session-01
        placement:    EXT
        flavor:       m1.small
        image:        ubuntu-16.04.1-x86_64
        minimum_size: 1
        maximum_size: 1
        default_size: 1
        interfaces:
          - name: oam
          - name: svc1
```

#VSLIDE

### 7. Add a virtual machine - `server1` (2)

```yml
topology_templates:
  node_templates:
   ...
   server1:
      ...
      capabilities:
        service1:
          properties:
            name:      ssh
            interface: oam
            ports:
              - protocol: tcp
                range:    '22'
        service2:
          properties:
            name:      web
            interface: svc1
            ports:
              - protocol: tcp
                range:    '80'
```

#VSLIDE

### 7. Add a virtual machine - `server1` (3)

```yml
topology_templates:
  node_templates:
   ...
   server1:
      ...
        service3:
          properties:
            name:      http_proxy
            interface: oam
            ports:
              - protocol: tcp
                range:    '3128'
        service4:
          properties:
            name:      dns
            interface: oam
            ports:
              - protocol: udp
                range:    '53'
```

#VSLIDE

### 7. Add virtual machines - `server2` (1)

Now, let's add our web server.

```yml
topology_templates:
  node_templates:
   ...
   server2:
      type: tosca.dtag.nodes.InternalComponent
      properties:
        name:         server2
        version:      '1.0'
        description:  server2 server
        tenant:       training-session-01
        placement:    INT
        flavor:       m1.small
        image:        ubuntu-16.04.1-x86_64
        minimum_size: 1
        maximum_size: 1
        default_size: 1
        volumes:
          - device: /dev/vdb1
            size:   10
            type:   INT
        interfaces:
          - name: oam
```

#VSLIDE

### 7. Add virtual machines - `server2` (2)

```yml
topology_templates:
  node_templates:
   ...
   server2:
      ...
      capabilities:
        service1:
          properties:
            name:      ssh
            interface: oam
            ports:
              - protocol: tcp
                range:    '22'
      requirements:
        - proxy_http_proxy:
            node_filter:
              properties:
              -  component:  server1
                 capability: http_proxy
                 interface:  oam
```

#VSLIDE

### 7. Add virtual machines - `server3` (1)

This machine allows SSH access to the other machines.

```yml
topology_templates:
  node_templates:
   ...
   server3:
      type: tosca.dtag.nodes.InternalComponent
      properties:
        name:         server3
        version:      '1.0'
        description:  server3 server
        tenant:       training-session-01
        placement:    MGMT
        flavor:       m1.medium
        image:        ubuntu-16.04.1-x86_64
        minimum_size: 1
        maximum_size: 1
        default_size: 1
        interfaces:
          - name: svc2
          - name: oam
```

#VSLIDE

### 7. Add virtual machines - `server3` (2)

```yml
topology_templates:
  node_templates:
   ...
   server3:
     capabilities:
       service1:
         properties:
           name:      ssh
           interface: svc2
           ports:
             - protocol: tcp
               range:    '22'
```

#VSLIDE

### 7. Add virtual machines - `server3` (3)

```yml
topology_templates:
  node_templates:
   ...
   server3:
     requirements:
        - server1_ssh:
            node_filter:
              properties:
              -  component:  server1
                 capability: ssh
                 interface:  oam
        - server2_ssh:
            node_filter:
              properties:
              -  component:  server2
                 capability: ssh
                 interface:  oam
```

#VSLIDE

A lot of typing for little result, ain't it?

<br>

__ Let's have a break! __

#HSLIDE

## 8.`generator` - From TOSCA to HOT

#VSLIDE

The `generator` is a Python script that
- reads the VNFD
- does some transformation magic and
- outputs HEAT scripts that you can deploy on our Openstack cloud.

#VSLIDE

### Phases

<!-- TODO: phases -->

1

2

3

4

#VSLIDE

### Syntax

```bash
usage: generator.py
        [-i INPUT]
        [-t TEMPLATE]
        [-o OUTPUT]
```

```bash
python generator.py -i vnfd.tosca.yaml -t templates/phase1.j2
```

#VSLIDE

###

#HSLIDE

## 9. Deployment

#VSLIDE

### Establish SSH connection

#VSLIDE

### Activate Openstack credentials

#VSLIDE

### Run Generator to create HOT files



#HSLIDE

## Your turn

#VSLIDE

Q: Which requirements towards the TOSCA description <br> for your applications can you think of?
