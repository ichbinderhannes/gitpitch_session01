#!/usr/bin/env python
from livereload import Server, shell
server = Server()
server.watch('PITCHME.md', shell('cp -r ./assets ./PITCHME/'))
server.serve(root='PITCHME/', port=8080)
